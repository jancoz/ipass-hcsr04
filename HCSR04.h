/*
Copyright (C) 2016  Jan Cozijnsen

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/// \mainpage HCSR04 Library Mainpage
///
/// Welcome to the documentation of the HCSR04 Library by Jan Cozijnsen.\n\n\n
/// Copyright (C) 2016  Jan Cozijnsen\n
///\n
/// This program is free software: you can redistribute it and/or modify\n
/// it under the terms of the GNU General Public License as published by\n
/// the Free Software Foundation, either version 3 of the License, or\n
/// (at your option) any later version.\n
///\n
///This program is distributed in the hope that it will be useful,\n
///but WITHOUT ANY WARRANTY; without even the implied warranty of\n
///MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n
///GNU General Public License for more details.\n
///\n
///You should have received a copy of the GNU General Public License\n
///along with this program.  If not, see <http://www.gnu.org/licenses/>.\n
/// /
#ifndef HCSR04
#define HCSR04


/// @file
/// HCSR04 Distance Sensor Library  file.\n


#include "hwlib.hpp"
#include <cstring>

///Units used in distancemeter and speedometer.
enum class Unit {Centim, Inch, Ms, Kmh, Mh, Cms, mHz, Hz};

/// Abstract class which is able to measure distance.
class distancemeter {
public:
	/// Empty constuctor.
	distancemeter() {}
	
	/// Virtual function which returns the distance from the sensor to an object in unit defined in parameter.
	//
	///The parameter "unit" is for specifying the output unit of this function.
	///For centimeters use "Unit::Centim".
	///For Inches use "Unit::Inch".
	///By default centimeters will be used.
	///Note: Please be aware that enumerations are case sensitive.
	virtual long long int measure(Unit unit = Unit::Centim) = 0;
};

/// Sub-class for distance sensor HCSR04 which is a sonar based distance sensor.
class hcsr04 : public distancemeter {
private:
	hwlib::target::pin_out & trig;
	hwlib::target::pin_in & echo;
	
public: 
	/// Constructor which has two pins as parameters, trig output pin and echo input pin.
	hcsr04(hwlib::target::pin_out & trig, hwlib::target::pin_in & echo):
		distancemeter(),
		trig(trig), 
		echo(echo) {}
	
	/// Distance measurement function for the HCSR04 sonar distance sensor. 
	long long int measure(Unit unit = Unit::Centim) {
		long long int timestamp1 = 0, timestamp2 = 0, timedif = 0;
		trig.set(0);
		hwlib::wait_ms(10);
		hwlib::now_us();
		trig.set(1);
		hwlib::wait_us(15);
		trig.set(0);
		while (echo.get() == 0) {}
		timestamp1 = hwlib::now_us();
		while (echo.get() == 1){}
		timestamp2 = hwlib::now_us();
		
		timedif = timestamp2 - timestamp1;
		switch (unit) {
			case Unit::Centim		: return timedif / 58;
			case Unit::Inch		: return timedif / 148;
			default : hwlib::cout << "ERROR: Entered unit not available in this function\n"; return 0;
		}
		return 0;
	}
};

/// Class which can create objects which can calculate speed. Requires object distance sensor.
class speedometer {
	
private:
	/// The distance sensor object.
	distancemeter & meter;
	
public:
	speedometer(distancemeter & meter):
		meter(meter) {}
	
		
	/// Calculates the speed of a moving object.
	// \n
	/// This function can output in 3 units by using the "unit" parameter.\n
	/// For meter per second use "Unit::Ms".\n
	/// For kilometers per hour use "Unit::Kmh".\n
	/// For miles per hour use "Unit::Mh".\n
	/// For centimeters per second use "Unit::Cms"\n
	/// The default unit is meter per second.\n
	/// Note: Be aware that enumerations are case sensitive.\n
	///\n
	/// The second parameter, int delay, is used for specifying the delay between the two distance measurements.\n
	/// The faster the object, the shorter the delay should be. \n
	/// The delay parameter is in milliseconds.
	long long int getSpeed(Unit unit = Unit::Ms, int delay = 10) {
		int timestamp = hwlib::now_us();
		long long int res1 = meter.measure();
		hwlib::wait_ms(delay);
		long long int res2 = meter.measure();
		int timedif = (hwlib::now_us() - timestamp) / 1000;
		long long int result = res1-res2;
		switch(unit) {
			case Unit::Ms	: return ((result*10) / timedif);
			case Unit::Kmh	: return ((result*36) / timedif);
			case Unit::Mh	: return ((result * 22) / timedif);
			case Unit::Cms : return ((result*1000) / timedif);
			default : hwlib::cout << "ERROR: Entered unit not available in this function\n"; return 0;
		}
		return 0;
	}
	
	/// Calculates the frequency of a moving object.
	//
	/// The parameter "int measurements" represents the amount of times the getSpeed() function will be called in total.\n
	/// The parameter "Freq unit" represents the output unit. Use "Unit::Hz for hertz and use "Unit::mHz" for millihertz.
	/// The parameter "int delay1" represents the delay between the getSpeed() calls.\n
	/// The parameter "int delay2" represents the delay between the two distance measurements inside the function getSpeed().\n
	int getFreq(int measurements, Unit unit = Unit::Hz, int delay1 = 10, int delay2 = 100) { ///TO BE TESTED
		int switches = 0;
		int timestamp = hwlib::now_us();
		bool status = getSpeed(Unit::Cms) > 0;
		for (int i = 0; i < measurements; i++) {
			if (status) {
				if (getSpeed(Unit::Cms, delay2) < 0) {
					status = 0;
					switches++;
				}
			}
			else {
				if (getSpeed(Unit::Cms, delay2) > 0) {
					status = 1;
					switches++;
				}
			}
			hwlib::wait_ms(delay1);
		}
		int time = (hwlib::now_us() - timestamp) / 1000;
		switch(unit) {
			case Unit::Hz : return switches / (time / 1000);
			case Unit::mHz : return (switches*1000) / (time /1000);
			default : hwlib::cout << "ERROR: Entered unit not available in this function\n"; return 0;
		}
		return 0;
	}
};

///Class solely for storing the character bitmaps, and information about these character bitmaps.
class disparray {
private:
	int bitmapsize[123] = {};
public: 
	hwlib::location bitmaps[123][20];
	
	disparray() {
		//Initialization of bitmapsize;
		bitmapsize[39] = 4;
		bitmapsize[45] = 8;
		bitmapsize[47] = 6;
		bitmapsize[48] = 12;
		bitmapsize[49] = 9;
		bitmapsize[50] = 16;
		bitmapsize[51] = 15;
		bitmapsize[52] = 13;
		bitmapsize[53] = 19;
		bitmapsize[54] = 18;
		bitmapsize[55] = 11;
		bitmapsize[56] = 18;
		bitmapsize[57] = 18;
		bitmapsize[72] = 16;
		bitmapsize[99] = 8;
		bitmapsize[104] = 11;
		bitmapsize[107] = 11;
		bitmapsize[109] = 12;
		bitmapsize[115] = 10;
		bitmapsize[122] = 10;
		
		hwlib::location b39[] = { //Array of the inch sign.
			hwlib::location(2, 1), hwlib::location(4, 1),
			hwlib::location(2, 2), hwlib::location(4, 2)
		};
		memcpy(bitmaps[39], b39, 20*sizeof(b39[0]));
		
		hwlib::location b45[] = { //Array of minus
			hwlib::location(2, 3), hwlib::location(3, 3), hwlib::location(4, 3), hwlib::location(5, 3),
			hwlib::location(2, 4), hwlib::location(3, 4), hwlib::location(4, 4), hwlib::location(5, 4)
		};
		memcpy(bitmaps[45], b45, 20*sizeof(b39[0]));
		
		hwlib::location b47[] = { //Array of the forward slash
			hwlib::location(6, 1),
			hwlib::location(5, 2),
			hwlib::location(4, 3),
			hwlib::location(3, 4),
			hwlib::location(2, 5),
			hwlib::location(1, 6)
		};
		memcpy(bitmaps[47], b47, 20*sizeof(b39[0]));
		
		hwlib::location b48[] = { //Array of zero, location 48
			hwlib::location(3, 1), hwlib::location(4, 1),
			hwlib::location(2, 2), hwlib::location(5, 2),
			hwlib::location(1, 3), hwlib::location(6, 3),
			hwlib::location(1, 4), hwlib::location(6, 4),
			hwlib::location(2, 5), hwlib::location(5, 5),
			hwlib::location(3, 6), hwlib::location(4, 6)
		};
		memcpy(bitmaps[48], b48, 20*sizeof(b39[0]));
		
		hwlib::location b49[] = { //Array of one
			hwlib::location(2, 1),
			hwlib::location(1, 2), hwlib::location(2, 2),
			hwlib::location(2, 3),
			hwlib::location(2, 4),
			hwlib::location(2, 5),
			hwlib::location(1, 6), hwlib::location(2, 6), hwlib::location(3, 6)
		};
		memcpy(bitmaps[49], b49, 20*sizeof(b39[0]));
		
		hwlib::location b50[] = { //Array of two
			hwlib::location(2, 1), hwlib::location(3, 1), hwlib::location(4, 1), hwlib::location(5, 1),
			hwlib::location(1, 2), hwlib::location(6, 2),
			hwlib::location(5, 3),
			hwlib::location(3, 4), hwlib::location(4, 4),
			hwlib::location(2, 5),
			hwlib::location(1, 6), hwlib::location(2, 6), hwlib::location(3, 6), hwlib::location(4, 6), hwlib::location(5, 6), hwlib::location(6, 6)
		};
		memcpy(bitmaps[50], b50, 20*sizeof(b39[0]));
		
		hwlib::location b51[] = { //Array of three
			hwlib::location(2, 1), hwlib::location(3, 1), hwlib::location(4, 1), hwlib::location(5, 1),
			hwlib::location(1, 2), hwlib::location(6, 2),
			hwlib::location(5, 3),
			hwlib::location(3, 4), hwlib::location(4, 4),
			hwlib::location(1, 5), hwlib::location(6, 5),
			hwlib::location(2, 6), hwlib::location(3, 6), hwlib::location(4, 6), hwlib::location(5, 6)
		};
		memcpy(bitmaps[51], b51, 20*sizeof(b39[0]));
		
		hwlib::location b52[] = { //Array of four
			hwlib::location(4, 1),
			hwlib::location(3, 2), hwlib::location(4, 2),
			hwlib::location(2, 3), hwlib::location(4, 3),
			hwlib::location(1, 4), hwlib::location(4, 4),
			hwlib::location(1, 5), hwlib::location(2, 5), hwlib::location(3, 5), hwlib::location(4, 5), hwlib::location(5, 5),
			hwlib::location(4, 6)
		};
		memcpy(bitmaps[52], b52, 20*sizeof(b39[0]));
		
		hwlib::location b53[] = { //Array of five
			hwlib::location(1, 1), hwlib::location(2, 1), hwlib::location(3, 1), hwlib::location(4, 1), hwlib::location(5, 1), hwlib::location(6,6),
			hwlib::location(1, 2),
			hwlib::location(1, 3), hwlib::location(2, 3), hwlib::location(3, 3), hwlib::location(4, 3), hwlib::location(5, 3),
			hwlib::location(6, 4),
			hwlib::location(1, 5), hwlib::location(6, 5),
			hwlib::location(2, 6), hwlib::location(3, 6), hwlib::location(4, 6), hwlib::location(5, 6)
		};
		memcpy(bitmaps[53], b53, 20*sizeof(b39[0]));
		
		hwlib::location b54[] = { //Array of six
			hwlib::location(2, 1), hwlib::location(3, 1), hwlib::location(4, 1), hwlib::location(5, 1),
			hwlib::location(1, 2),
			hwlib::location(1, 3), hwlib::location(2, 3), hwlib::location(3, 3), hwlib::location(4, 3), hwlib::location(5, 3),
			hwlib::location(1, 4), hwlib::location(6, 4),
			hwlib::location(1, 5), hwlib::location(6, 5),
			hwlib::location(2, 6), hwlib::location(3, 6), hwlib::location(4, 6), hwlib::location(5, 6)
		};
		memcpy(bitmaps[54], b54, 20*sizeof(b39[0]));
		
		hwlib::location b55[] = { //Array of seven
			hwlib::location(1, 1), hwlib::location(2, 1), hwlib::location(3, 1), hwlib::location(4, 1), hwlib::location(5, 1), hwlib::location(6, 1),
			hwlib::location(5, 2),
			hwlib::location(4, 3),
			hwlib::location(3, 4),
			hwlib::location(2, 5),
			hwlib::location(1, 6)
		};
		memcpy(bitmaps[55], b55, 20*sizeof(b39[0]));
		
		hwlib::location b56[] = { //Array of eight
			hwlib::location(2, 6), hwlib::location(3, 6), hwlib::location(4, 6), hwlib::location(5, 6),
			hwlib::location(1, 5), hwlib::location(6, 5),
			hwlib::location(2, 4), hwlib::location(3, 4), hwlib::location(4, 4), hwlib::location(5, 4),
			hwlib::location(1, 3), hwlib::location(6, 3),
			hwlib::location(1, 2), hwlib::location(6, 2),
			hwlib::location(2, 1), hwlib::location(3, 1), hwlib::location(4, 1), hwlib::location(5, 1)
		};
		memcpy(bitmaps[56], b56, 20*sizeof(b39[0]));
		
		hwlib::location b57[] = { //Array of nine, location 57
			hwlib::location(2, 1), hwlib::location(3, 1), hwlib::location(4, 1), hwlib::location(5, 1),
			hwlib::location(1, 2), hwlib::location(6, 2),
			hwlib::location(1, 3), hwlib::location(6, 3),
			hwlib::location(2, 4), hwlib::location(3, 4), hwlib::location(4, 4), hwlib::location(5, 4), hwlib::location(6, 4),
			hwlib::location(6, 5),
			hwlib::location(2, 6), hwlib::location(3, 6), hwlib::location(4, 6), hwlib::location(5, 6)
		};
		memcpy(bitmaps[57], b57, 20*sizeof(b39[0]));
		
		hwlib::location b72[] = { //Array of the letter "H"
			hwlib::location(1, 1), hwlib::location(6, 1),
			hwlib::location(1, 2), hwlib::location(6, 2),
			hwlib::location(1, 3), hwlib::location(2, 3), hwlib::location(3, 3), hwlib::location(4, 3), hwlib::location(5, 3), hwlib::location(6, 3),
			hwlib::location(1, 4), hwlib::location(6, 4),
			hwlib::location(1, 5), hwlib::location(6, 5),
			hwlib::location(1, 6), hwlib::location(6, 6)
		};
		memcpy(bitmaps[72], b72, 20*sizeof(b39[0]));
		
		hwlib::location b99[] = { //Array of the letter "c", only 4 in length
			hwlib::location(2, 3), hwlib::location(3, 3), hwlib::location(4, 3),
			hwlib::location(1, 4),
			hwlib::location(1, 5),
			hwlib::location(2, 6), hwlib::location(3, 6), hwlib::location(4, 6)
		};
		memcpy(bitmaps[99], b99, 20*sizeof(b39[0]));
		
		hwlib::location b104[] = { //Array of the letter "h", only 4 in length
			hwlib::location(1, 1),
			hwlib::location(1, 2),
			hwlib::location(1, 3), hwlib::location(2, 3), hwlib::location(3, 3),
			hwlib::location(1, 4), hwlib::location(4, 4),
			hwlib::location(1, 5), hwlib::location(4, 5),
			hwlib::location(1, 6), hwlib::location(4, 6)
		};
		memcpy(bitmaps[104], b104, 20*sizeof(b39[0]));
		
		hwlib::location b107[] = { //Array of the letter "k", only 4 in length
			hwlib::location(1, 1),
			hwlib::location(1, 2),
			hwlib::location(1, 3), hwlib::location(4, 3),
			hwlib::location(1, 4), hwlib::location(3, 4),
			hwlib::location(1, 5), hwlib::location(2, 5),
			hwlib::location(1, 6), hwlib::location(3, 6), hwlib::location(4, 6)
		};
		memcpy(bitmaps[107], b107, 20*sizeof(b39[0]));
		
		hwlib::location b109[] = { //Array of the letter "m"
			hwlib::location(1, 3), hwlib::location(2, 3), hwlib::location(5, 3), hwlib::location(6, 3),
			hwlib::location(1, 4), hwlib::location(3, 4), hwlib::location(4, 4), hwlib::location(6, 4),
			hwlib::location(1, 5), hwlib::location(6, 5),
			hwlib::location(1, 6), hwlib::location(6, 6)
		};
		memcpy(bitmaps[109], b109, 20*sizeof(b39[0]));
		
		hwlib::location b115[] = { //Array of the letter "s", only 4 in length.
			hwlib::location(2, 2), hwlib::location(3, 2), hwlib::location(4, 2),
			hwlib::location(1, 3),
			hwlib::location(2, 4), hwlib::location(3, 4),
			hwlib::location(3, 5),
			hwlib::location(1, 6), hwlib::location(2, 6), hwlib::location(3, 6)
		};
		memcpy(bitmaps[115], b115, 20*sizeof(b39[0]));	
		
		hwlib::location b122[] = { //Array of the letter 'z'.
		hwlib::location(1, 3), hwlib::location(2, 3), hwlib::location(3, 3), hwlib::location(4, 3),
		hwlib::location(3, 4),
		hwlib::location(2, 5),
		hwlib::location(1, 6), hwlib::location(2, 6), hwlib::location(3, 6), hwlib::location(4, 6),
		};
		memcpy(bitmaps[122], b122, 20*sizeof(b39[0]));
	}
		
	///Function for requesting the bitmap size of a certain character.
	int getSize(char c) {
		int value = c;
		return bitmapsize[value];
	}
};

///Class for displaying certain data on a display.
class display {
private:
	hwlib::window & w;
public:
	display(hwlib::window & w):
		w(w)
		{}
	
	///Function which display a percentage in the form of a percentage bar in horizontal direction.
	//
	///"int perc" is the percentage you want to display, from 0 to 100.\n
	///"int width" is the percentage of the width, so if you put in 100 the width of the percentage bar will be 100% the width of the display.
	void dispPerc(int perc, int width) {
		w.clear();
		for (int i = 1; i < ((41*width)/64); i++) {
			for (int j = 1; j < ((perc*164)/128); j++) {
				w.write(hwlib::location(j,i), hwlib::black);
			}
		}
	}
	
	
	///This function displays an integer on the display. 
	//
	///The parameter "int number" is the number that will be display, it can be a maximum of 3 characters.\n\n
	///The parameter "int height" specifies how high on the display the text will be displayed. 0 is the middle of the display. \n
	void dispNumber(int number, int height) {
		disparray arrays;
		hwlib::location heightcorr(0, 29+height);
		if (number == 0) {
			for (int i = 0; i < arrays.getSize('0'); i++) {
				w.write(arrays.bitmaps['0'][i] + heightcorr, hwlib::black);
			}
			return;
		}
		int digits = 0;
		bool neg = 0;
		int a[4] = { 0,0,0,0 };
		if (number < 0) {
			neg = 1;
			number *= -1;
		}
		if ((number / 1000) > 0) {
			a[3] = number % 10;
			number /= 10;
			a[2] = number % 10;
			number /= 10;
			a[1] = number % 10;
			number /= 10;
			a[0] = number;
			digits = 4;
		}
		else if ((number / 100) > 0) {
			a[2] = number % 10;
			number /= 10;
			a[1] = number % 10;
			number /= 10;
			a[0] = number;
			digits = 3;
		}
		else if ((number / 10) > 0) {
			a[1] = number % 10;
			number /= 10;
			a[0] = number;
			digits = 2;
		}
		else {
			a[0] = number;
			digits = 1;
		}
		int xcorrection = 0;
		if (neg) {
			for (int i = 0; i < arrays.getSize('-'); i++) {
				w.write(arrays.bitmaps['-'][i] + heightcorr, hwlib::black);
			}
			xcorrection += 8;
		}
		for (int i = 0; i < digits; i++) {
			for (int j = 0; j < arrays.getSize(a[i] + '0'); j++) {
				w.write(arrays.bitmaps[a[i] + '0'][j] + heightcorr + hwlib::location(xcorrection, 0), hwlib::black);
			}
			xcorrection += 8;
		}
	}
	
	
	///This function displays a number and a unit.\n\n
	//
	///"int number" is the number being displayed.\n
	///"char unit[]" is the a string of the unit being displayed after the number.\n
	///"char unit[]" can only consist of characters available in the bitmap arrays in disparray::bitmaps.\n
	///Be aware that in correct C++ you need to cast the string, using (char*) in front of the string like: (char*)"m/s"\n
	///"int height" is the height of the printed text, from the middle of the display.
	void dispNumberUnit(int number, char unit[], int height) {
		disparray arrays;
		hwlib::location heightcorr(0, 29+height);
		bool zero = 0;
		if (number == 0) {
			zero = 1;
		}
		int digits = 0;
		bool neg = 0;
		int a[4] = { 0,0,0,0 };
		if (number < 0) {
			neg = 1;
			number *= -1;
		}
		if ((number / 1000) > 0) {
			a[3] = number % 10;
			number /= 10;
			a[2] = number % 10;
			number /= 10;
			a[1] = number % 10;
			number /= 10;
			a[0] = number;
			digits = 4;
		}
		else if ((number / 100) > 0) {
			a[2] = number % 10;
			number /= 10;
			a[1] = number % 10;
			number /= 10;
			a[0] = number;
			digits = 3;
		}
		else if ((number / 10) > 0) {
			a[1] = number % 10;
			number /= 10;
			a[0] = number;
			digits = 2;
		}
		else {
			a[0] = number;
			digits = 1;
		}
		int xcorrection = 0;
		if (neg) {
			for (int i = 0; i < arrays.getSize('-'); i++) {
				w.write(arrays.bitmaps['-'][i] + heightcorr, hwlib::black);
			}
			xcorrection += 8;
		}
		if (!zero) {
			for (int i = 0; i < digits; i++) {
				for (int j = 0; j < arrays.getSize(a[i] + '0'); j++) {
					w.write(arrays.bitmaps[a[i] + '0'][j] + heightcorr + hwlib::location(xcorrection, 0), hwlib::black);
				}
				xcorrection += 8;
			}
		}
		else {
			for (int i = 0; i < arrays.getSize('0'); i++) {
				w.write(arrays.bitmaps['0'][i] + heightcorr, hwlib::black);
			}
			xcorrection += 8;
		}
		xcorrection += 2;
		
		for (int i = 0; unit[i] != '\0'; i++) {
			for (int j = 0; j < arrays.getSize(unit[i]); j++) {
				int character = unit[i];
				w.write(arrays.bitmaps[character][j] + heightcorr + hwlib::location(xcorrection, 0), hwlib::black);
			}
			xcorrection += 8;
		}
	}
};

///Controller superclass.
//
///The controller is mainly intended for displaying measurements on the display but can also be used for fetching results.
class controller {
protected:
	display & screen;
public:
	///Superclass constructor.
	controller(display & screen):
		screen(screen) {}
	
	///Function for fetching result.
	//
	///"Unit unit" represents the unit in which the measurements will be outputted.\n
	virtual int getResult(Unit unit) = 0;
	
	///Function for displaying result directly on display.
	//
	///"Unit unit" represents the unit in which the measurements will be outputted.\n
	virtual void dispResult(Unit unit, int height) = 0;
	
};

///Distance meter controller.
//
///This controller is mainly intended for displaying measurements on the display, it can also be used for just fetching results.
class contrDist : public controller {
private:
	distancemeter & meter;
public:
	contrDist(distancemeter & meter, display & screen):
		controller(screen), meter(meter) {}
	
	///Function for fetching result.
	//
	///"Unit unit" represents the unit in which the measurements will be outputted.\n
	///Available units:\n
	///Unit::Centim for centimeters.\n
	///Unit::Inch for inches.\n
	
	int getResult(Unit unit) {
		return meter.measure(unit);
	}
	
	///Function for displaying result directly on display.
	//
	///"Unit unit" represents the unit in which the measurements will be outputted.\n
	///Available units:\n
	///Unit::Centim for centimeters.\n
	///Unit::Inch for inches.\n
	void dispResult(Unit unit, int height) {
		int result = meter.measure(unit);
		if (result > 1000) {
			hwlib::cout << "WARNING: Inaccurate measurement detected!\n";
			result = 0;
		}
		switch(unit) {
			case Unit::Centim : screen.dispNumberUnit(result, (char*)"cm", height); return;
			case Unit::Inch : screen.dispNumberUnit(result, (char*)"\'", height); return;
			default : hwlib::cout << "ERROR: Entered unit not available in this function\n"; return;
		}
	}
};

///Speedometer controller.
class contrSpeed : public controller {
private:
	speedometer & meter;
	int delay = 10;
public:
	contrSpeed(speedometer & meter, display & screen):
		controller(screen), meter(meter) {}
		
	///Function for fetching result.
	//
	///"Unit unit" represents the unit in which the measurements will be outputted.\n
	///Available units: \n
	///"Unit::Ms" for meter per second.\n
	///"Unit::Kmh" for kilometers per hour.\n
	///"Unit::Mh" for miles per hour.\n
	///"Unit::Cms: for centimeters per hour.\n
	int getResult(Unit unit) {
		return meter.getSpeed(unit, delay);
	}
	
	///Function for displaying result directly on display.
	//
	///"Unit unit" represents the unit in which the measurements will be outputted.\n
	///"int height" represents the height of the text printed on the display, 0 is the middle of the display.\n
	///Available units: \n
	///"Unit::Ms" for meter per second.\n
	///"Unit::Kmh" for kilometers per hour.\n
	///"Unit::Mh" for miles per hour.\n
	///"Unit::Cms: for centimeters per hour.\n
	void dispResult(Unit unit, int height) {
		int result = meter.getSpeed(unit, delay);
		switch(unit) {
			case Unit::Cms : screen.dispNumberUnit(result, (char*)"cm/s", height); return;
			case Unit::Kmh : screen.dispNumberUnit(result, (char*)"km/h", height); return;
			case Unit::Mh : screen.dispNumberUnit(result, (char*)"m/h", height); return;
			case Unit::Ms : screen.dispNumberUnit(result, (char*)"m/s", height); return;
			default : hwlib::cout << "ERROR: Entered unit not available in this function\n"; return;
		}
	}
	
	
	///Function for setting the delay between the two distance measurements inside getSpeed().
	//
	///Delay is in milliseconds.
	void setDelay(int pdelay) {
		delay = pdelay;
	}
};

///Frequency meter controller.
class contrFreq : public controller {
private:
	speedometer & meter;
	int delay1 = 100;
	int delay2 = 10;
	int measurements = 50;
public:
	contrFreq(speedometer & meter, display & screen):
		controller(screen), meter(meter) {}
	
	///Function for fetching result..
	//
	///"Unit unit" represents the unit in which the measurements will be outputted.\n
	///"int height" represents the height of the text printed on the display, 0 is the middle of the display.\n
	///Available units: \n
	///"Unit::Hz" for hertz.\n
	///"Unit::mHz" for millihertz.\n
	int getResult(Unit unit) {
		return meter.getFreq(measurements, unit, delay1, delay2);
	}
	
	///Function for displaying result directly on display.
	//
	///"Unit unit" represents the unit in which the measurements will be outputted.\n
	///"int height" represents the height of the text printed on the display, 0 is the middle of the display.\n
	///Available units: \n
	///"Unit::Hz" for hertz.\n
	///"Unit::mHz" for millihertz.\n
	void dispResult(Unit unit, int height) {
		int result = meter.getFreq(measurements, unit, delay1, delay2);
		switch(unit) {
			case Unit::Hz : screen.dispNumberUnit(result, (char*)"Hz", height); return;
			case Unit::mHz : screen.dispNumberUnit(result, (char*)"mHz", height); return;
			default : hwlib::cout << "ERROR: Entered unit not available in this function\n"; return;
		}
	}
	
	///Function for setting the delay between getSpeed() calls.
	void setDelay1 (int delay) {
		delay1 = delay;
	}
	
	///Function for setting the delay inside the getSpeed() function, between the two distance measurements.
	void setDelay2 (int delay) {
		delay2 = delay;
	}
	
	///Function for setting the amount of times getSpeed() is called during getResult() or dispResult().
	void setMeasurements (int mes) {
		measurements = mes;
	}
};


#endif // HCSR04