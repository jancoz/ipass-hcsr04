var hierarchy =
[
    [ "controller", "classcontroller.html", [
      [ "contrDist", "classcontr_dist.html", null ],
      [ "contrFreq", "classcontr_freq.html", null ],
      [ "contrSpeed", "classcontr_speed.html", null ]
    ] ],
    [ "disparray", "classdisparray.html", null ],
    [ "display", "classdisplay.html", null ],
    [ "distancemeter", "classdistancemeter.html", [
      [ "hcsr04", "classhcsr04.html", null ]
    ] ],
    [ "speedometer", "classspeedometer.html", null ]
];