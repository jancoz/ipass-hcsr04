var annotated_dup =
[
    [ "contrDist", "classcontr_dist.html", "classcontr_dist" ],
    [ "contrFreq", "classcontr_freq.html", "classcontr_freq" ],
    [ "controller", "classcontroller.html", "classcontroller" ],
    [ "contrSpeed", "classcontr_speed.html", "classcontr_speed" ],
    [ "disparray", "classdisparray.html", "classdisparray" ],
    [ "display", "classdisplay.html", "classdisplay" ],
    [ "distancemeter", "classdistancemeter.html", "classdistancemeter" ],
    [ "hcsr04", "classhcsr04.html", "classhcsr04" ],
    [ "speedometer", "classspeedometer.html", "classspeedometer" ]
];