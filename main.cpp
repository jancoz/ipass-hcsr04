#include "hwlib.hpp"
#include "HCSR04.h"

int main() {
	hwlib::wait_ms(500);
	hwlib::target::pin_out trig (hwlib::target::pins::d3);
	hwlib::target::pin_in echo (hwlib::target::pins::d2);
	hcsr04 distanceobject(trig, echo);
	speedometer speedoobject(distanceobject);
	//display init
	auto scl = hwlib::target::pin_oc{ hwlib::target::pins::d9 };
	auto sda = hwlib::target::pin_oc{ hwlib::target::pins::d10 };
	auto i2c_bus = hwlib::i2c_bus_bit_banged_scl_sda{ scl,sda };
	auto screen = hwlib::glcd_oled{ i2c_bus, 0x3c };
	
	display theobject(screen);
	disparray info;
	contrFreq control(speedoobject, theobject);
	control.setMeasurements(100);
	while (1) {
		screen.clear();
		control.dispResult(Unit::Hz, 0);
		hwlib::wait_ms(1000);
	}
	//screen.clear();
	//theobject.dispNumberUnit(25, (char*)"kmh", 0);
}